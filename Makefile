all: build docker

JOB ?= "master-thesis"

BUILD_COMMAND = latexmk -xelatex -synctex=1 -jobname=${JOB} main.tex
CLEAN_COMMAND := rm -rf *.aux \
	*.fdb_latexmk \
	*.fls \
	*.lof \
	*.lot \
	*.log \
	*.out \
	*.synctex.gz \
	*.toc

COMMAND=${BUILD_COMMAND}; ${CLEAN_COMMAND}


clean:
	${CLEAN_COMMAND}

docker:
	docker run --rm -ti -v ${PWD}:/master-thesis:Z docker-latex bash -c "${COMMAND}"

#build:
#    make build_practice ;  make build_diploma

build_diploma:
	make PWD="${PWD}/diploma" docker

build_practice:JOB := "practice"
build_practice:
	make PWD="${PWD}/practice"  docker
